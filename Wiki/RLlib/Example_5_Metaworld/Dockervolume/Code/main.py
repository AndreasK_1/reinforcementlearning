# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% IMPORTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
import os
print(os.environ.get("LD_LIBRARY_PATH", ""))
import metaworld
import random
import ray
from ray import tune
from ray.rllib.agents.registry import get_trainer_class
from ray.rllib.env.env_context import EnvContext
import gym
import ray.rllib.agents

#print(metaworld.ML1.ENV_NAMES) # Names of available environments


# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Environment creation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# Metaworld and Gym environments are per default not compatible with RLlib
# like documented here: https://docs.ray.io/en/latest/rllib/rllib-env.html
# Therefore an adjusted environment class is created
class Metaworld(gym.Env):

  def __init__(self, config: EnvContext):
    self.ml1 = metaworld.ML1('button-press-topdown-v2')  # Construct the benchmark, sampling tasks
    self.env = self.ml1.train_classes['button-press-topdown-v2']()  # Create an environment with sampling task
    self.task = random.choice(self.ml1.train_tasks)  # Choose task
    self.env.set_task(self.task)  # Set task
    self.action_space = self.env.action_space # Get action space
    self.observation_space = self.env.observation_space # Get obeservation space
    self.episode_reward = 0
  def reset(self):
    self.done = False
    return self.env.reset()

  def render(self):
    self.env.render()

  def step(self, action):
    self.env.obs, self.env.reward, self.env.done, self.env.info = self.env.step(action)
    if self.env.curr_path_length > self.env.max_path_length:  # If to many steps -> reset env
        self.env.done = True
        self.env.reset()

    return self.env.obs, self.env.reward, self.env.done, self.env.info

# %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Environment testing %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# env=Metaworld(config={})
# episode_reward=0
# for iterations in range(3):
#     done = False
#     episode_reward = 0
#     while not done:
#       env.render()
#       action = env.action_space.sample()  # Sample an action
#       obs, reward, done, info = env.step(action)  # Step the environoment with the sampled random action
#       episode_reward += reward
#     print(episode_reward)

# %%%%%%%%%% Environment training %%%%%%%%%%
# Initialise ray framework
ray.init()

# Configuration for the RL training
# https://docs.ray.io/en/latest/rllib-training.html#common-parameters
config = {
    "num_workers": 7,
    "env": Metaworld,
    "num_gpus": 1,
    "framework": "tf2",
    "eager_tracing": True,
    "evaluation_interval": 1,
    "evaluation_duration": 1,
}
# Which algorithm should be used.
# https://docs.ray.io/en/latest/rllib-algorithms.html

# ray.rllib.agents.ppo.PPOTrainer(config={
#     "vf_clip_param": 1000.0,
# })
algorithm = "PPO"
# %%%%%%%%%% Training %%%%%%%%%%
# results = tune.run(
#     algorithm,
#     stop={"training_iteration": 12},
#     config=config,
#     local_dir="/Dockervolume/Logs",
#     checkpoint_freq = 3,
#     checkpoint_at_end=True,
# )


# %%%%%%%%%% Evaluation %%%%%%%%%%
print("Training completed. Restoring new Trainer for action inference. Average reward should be 1000-1500")
# Get the last checkpoint from the above training run.
#checkpoint = results.get_last_checkpoint()
# Saved checkpoint with trained agent for 60 iterations
checkpoint = "/Dockervolume/Logs/PPO/PPO_Metaworld_37a48_00000_0_2022-03-10_13-37-23/checkpoint_000060/checkpoint-60"
# Create new RL trainer/agent and restore its state from the last checkpoint.
config["num_workers"]=0
trainer = get_trainer_class(algorithm)(config=config)
trainer.restore(checkpoint)
# Create an environment to try out the agent in.
for iterations in range(3):
    env = Metaworld(config={})
    obs = env.reset()
    # Give the agent 3 trials
    done = False
    episode_reward = 0

    done = False
    episode_reward = 0
    while not done:
        # Uncomment if visualisation is necessary
        env.render()
        # Compute an action (`a`).
        a = trainer.compute_single_action(
            observation=obs
        )
        # Send the computed action `a` to the environment.
        obs, reward, done, _ = env.step(a)
        episode_reward += reward
        # Is the episode `done`? -> Reset.
        if done:
            print(f"Episode done: Total reward = {episode_reward}")
            obs = env.reset()
            episode_reward = 0.0
