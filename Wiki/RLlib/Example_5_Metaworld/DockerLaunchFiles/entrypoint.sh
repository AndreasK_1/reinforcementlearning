#!/usr/bin/env bash
echo 'Starting bash execution in Docker container terminal ...'

echo 'Current framework versions are:'
python3 --version

echo 'Starting python script ...'

python3 /usr/local/bin/main.py
