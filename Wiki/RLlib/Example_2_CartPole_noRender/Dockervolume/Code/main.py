import gym

# Create exemplary environment
env = gym.make("CartPole-v1")
obs = env.reset()

# Run the environment for 20 trials
for iterations in range(3):
    # Initialise
    num_episodes = 0
    episode_reward = 0.0
    done = False
    # Start environment loop
    while not done:
        # Take an environment step wit a random action
        obs, reward, done, _ = env.step(env.action_space.sample())
        # Calculate cummultative reward
        episode_reward += reward
        # Is the episode `done`? -> Reset.
        if done:
            print(f"Episode done: Total reward = {episode_reward}")
            obs = env.reset()
            num_episodes += 1
            episode_reward = 0.0


