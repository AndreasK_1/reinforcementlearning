# %%%%%%%%%% Imports %%%%%%%%%%
import gym
import ray
from ray import tune
from ray.rllib.agents.registry import get_trainer_class

# %%%%%%%%%% Environment setup %%%%%%%%%%
# Initialise ray framework
ray.init()
# Configuration for the RL training
# https://docs.ray.io/en/latest/rllib-training.html#common-parameters
config = {
    "num_workers": 2,
    "env": "CartPole-v1",
    "num_gpus": 1,
    "framework": "tf2",
    "eager_tracing": True,
    "evaluation_interval": 1,
    "evaluation_duration": 1,
    "evaluation_config": {
        "render_env": True,
    },
}
# Which algorithm should be used.
# https://docs.ray.io/en/latest/rllib-algorithms.html
algorithm = "PPO"

# %%%%%%%%%% Training %%%%%%%%%%
results = tune.run(
    algorithm,
    stop={"episode_reward_mean": 300},
    config=config,
    local_dir="/root/ray_results/PPO",
    checkpoint_at_end=True,
)

# %%%%%%%%%% Evaluation %%%%%%%%%%
print("Training completed. Restoring new Trainer for action inference.")
# Get the last checkpoint from the above training run.
checkpoint = results.get_last_checkpoint()
# Create new RL trainer/agent and restore its state from the last checkpoint.
trainer = get_trainer_class(algorithm)(config=config)
trainer.restore(checkpoint)
# Create an environment to try out the agent in.
env = gym.make(config["env"])
obs = env.reset()
# Give the agent 3 trials
for iterations in range(3):
    num_episodes = 0
    episode_reward = 0.0
    done = False
    while not done:
        env.render()
        # Compute an action (`a`).
        a = trainer.compute_single_action(
            observation=obs
        )
        # Send the computed action `a` to the environment.
        obs, reward, done, _ = env.step(a)
        episode_reward += reward
        # Is the episode `done`? -> Reset.
        if done:
            print(f"Episode done: Total reward = {episode_reward}")
            obs = env.reset()
            num_episodes += 1
            episode_reward = 0.0
