# %%%%%%%%%% Imports %%%%%%%%%%
import gym
import ray
from ray import tune
from ray.rllib.agents.registry import get_trainer_class
from gym import envs

# %%%%%%%%%% Environment setup %%%%%%%%%%
# Print all available environments
all_envs = envs.registry.all()
env_ids = [env_spec.id for env_spec in all_envs]
#for id in sorted(env_ids):
    #print(id)

# Initialise ray framework
ray.init()
# Configuration for the RL training
# https://docs.ray.io/en/latest/rllib-training.html#common-parameters
config = {
    "num_workers": 7,
    "env": "Pendulum-v1",
    "num_gpus": 1,
    "framework": "tf2",
    "eager_tracing": True,
    # Uncomment for render during iterations
    #"evaluation_config": {
    #    "render_env": True,
    #},
}
# Which algorithm should be used.
# https://docs.ray.io/en/latest/rllib-algorithms.html
algorithm = "PPO"

# %%%%%%%%%% Training %%%%%%%%%%
results = tune.run(
    algorithm,
    stop = {"training_iteration": 3},
    #stop={"episode_reward_mean": 350},
    config=config,
    local_dir="/Dockervolume/Logs",
    checkpoint_at_end=True,
)

# %%%%%%%%%% Evaluation %%%%%%%%%%
print("Training completed. Restoring new Trainer for action inference.")
# Get the last checkpoint from the above training run.
checkpoint = results.get_last_checkpoint()
# Create new RL trainer/agent and restore its state from the last checkpoint.
trainer = get_trainer_class(algorithm)(config=config)
trainer.restore(checkpoint)
# Create an environment to try out the agent in.
env = gym.make(config["env"])
obs = env.reset()
# Give the agent 3 trials
for iterations in range(3):
    num_episodes = 0
    episode_reward = 0.0
    done = False
    while not done:
        env.render()
        # Compute an action (`a`).
        a = trainer.compute_single_action(
            observation=obs
        )
        # Send the computed action `a` to the environment.
        obs, reward, done, _ = env.step(a)
        episode_reward += reward
        # Is the episode `done`? -> Reset.
        if done:
            print(f"Episode done: Total reward = {episode_reward}")
            obs = env.reset()
            num_episodes += 1
            episode_reward = 0.0
