#!/usr/bin/env bash
echo 'Hello World! This is terminal output from inside a docker container.'
echo 'My Python version:'
python3 --version
echo 'My GPU:'
nvidia-smi --query-gpu=name --format=csv,noheader
python3 ./Dockervolume/Code/main.py

