# -*- coding: utf-8 -*-
"""DirectoryScanner

This is a search function that takes the path to a base folder
and recursively walks through the directory subtree,
applying a predicate to each directory.
If the predicate returns True, the path to that directory is collected,
no further subdirectories of that path are visited and the search continues.


"""
# %%%%%%%%%% IMPORTS %%%%%%%%%%
import os
import random

class DirectoryScanner:

    def __init__(self, path_to_base_folder):
        self.path_to_base_folder = path_to_base_folder
        self.random_iterator = 0
    def scan_directories(self):
        """Function to scan directory and subdirectory

            Args:
                path_to_base_folder (str): starting directory for search
                predicate (function): Predicate function that evaluates path and returns True or False
            Returns:
                bool: True for "go into subdirectory", False otherwise.
        """
        # Every iteration of os.walk() lists current directory as well as to-be-searched subdirectories
        for dirName, subDirList, files in os.walk(self.path_to_base_folder, topdown=True):
            # Iterate over subdirectories to see if some have to be skipped
            for index, subDirName in enumerate(subDirList):
                # Should subdirectory be searched? -> predicate() decides this
                skipping_subdir = self.predicate(os.path.join(dirName, subDirName))
                # If predicate() returns True the subdirectory will not be searched further
                if skipping_subdir == False:
                    print(f"Predicate returned True, path saved, skipping this directroy {os.path.join(dirName, subDirName)}")
                    # Delete subdirectory from to-be-searched subdirectories
                    del subDirList[index]


    def predicate(self, path):
        """Predicate function returns either True or False based on path input

           Args:
               path (str): directory path on which predicate should be evaluated

           Returns:
               Bool (bool): False for "go into subdirectory", True for skipping and saving path of directory.
               (In this case the function returns true or false on a 50/50 semi-random chance)
        """
        # Access global random seed variable for repeatable results
        random.seed(self.random_iterator)
        # Increase random seed after each call of predicate()
        self.random_iterator += 1
        # Decide if true or false should be returned
        if random.random() > 0.5:
            return False
        else:
            return False


if __name__ == "__main__":
    # myScanner = DirectoryScanner("/Dockervolume")
    # myScanner.scan_directories()
    print("Starting scanner...")
