import directory_scanner as ds
import os

tmp_path = "/Dockervolume"



# content of test_tmp_path.py
CONTENT = "content"


def test_create_file(tmp_path):
    # Create x directories with each y subdirectories and
    for iterator in range(0,5):
        d = tmp_path / f"dirTest{iterator}"
        print(d)
        d.mkdir()
        p = d / "hello.txt"
        p.write_text(CONTENT)

    for root, dirs, files in os.walk(tmp_path, topdown=False):
        for name in dirs:
            print(f"Found directories: {os.path.join(root, name)}")
        for name in files:
            print(f"Found files: {os.path.join(root, name)}")

    #myScanner = ds.DirectoryScanner(tmp_path)
    #myScanner.scan_directories()

    assert p.read_text() == CONTENT
    assert len(list(tmp_path.iterdir())) == 5

