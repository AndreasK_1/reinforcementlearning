#!/usr/bin/env bash
echo 'Hello World! This is terminal output from inside a docker container.'
echo 'My Python version:'
python3 --version
echo 'My GPU:'
nvidia-smi --query-gpu=name --format=csv,noheader
#xauth add festo/unix:0  MIT-MAGIC-COOKIE-1  a27109bf3614bfdbcaf4e6384b501e0e
python3 ./Dockervolume/Code/main.py

