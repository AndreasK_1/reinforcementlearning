# This example shows all classic control and box2d environments and renders all of them

# %%%%%%%%%% Imports %%%%%%%%%%
import gym
import ray
from ray import tune
from ray.rllib.agents.registry import get_trainer_class

# %%%%%%%%%% Environment setup %%%%%%%%%%

# Show all gym environments
all_envs = gym.envs.registry.all()
env_ids = [env_spec.id for env_spec in all_envs]
print(env_ids)
# Working renderable environments
# %%% Classic control %%%
# 'CartPole-v0', 'CartPole-v1', 'MountainCar-v0', 'MountainCarContinuous-v0', 'Pendulum-v1', 'Acrobot-v1',
# %%% Box2d %%%
# 'LunarLander-v2', 'LunarLanderContinuous-v2', 'BipedalWalker-v3', 'BipedalWalkerHardcore-v3', 'CarRacing-v0'

work_env = ['CartPole-v0', 'CartPole-v1', 'MountainCar-v0', 'MountainCarContinuous-v0', 'Pendulum-v1', 'Acrobot-v1',
            'LunarLander-v2', 'LunarLanderContinuous-v2', 'BipedalWalker-v3', 'BipedalWalkerHardcore-v3', 'CarRacing-v0']

# Iterate over all classic control and box2d environments
for environment in work_env:
    # Initialise env
    env = gym.make(environment)
    episode_reward = 0.0
    step = 0
    env.reset()
    # Start env for 100 steps
    for t in range(100):
        env.render(mode="human")
        # Take random step
        obs, reward, done, _ = env.step(env.action_space.sample())
        episode_reward += reward
        # Is the episode `done`? -> Reset.
        if done:
            print(f"Episode done: Total reward = {episode_reward}")
            obs = env.reset()
            episode_reward = 0.0

